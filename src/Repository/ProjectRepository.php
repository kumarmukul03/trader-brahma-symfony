<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\ORM\EntityRepository;

class ProjectRepository extends EntityRepository
{
    public function loadProjectByUserId($userid): array
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery();

        return $qb->execute();
    }
}
