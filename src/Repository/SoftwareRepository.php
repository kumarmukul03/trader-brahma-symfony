<?php

namespace App\Repository;

use App\Entity\Software;
use Doctrine\ORM\EntityRepository;

class SoftwareRepository extends EntityRepository
{
    public function loadSoftwareByUserId($userid): array
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery();

        return $qb->execute();
    }
}
