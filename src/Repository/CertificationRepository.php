<?php

namespace App\Repository;

use App\Entity\Certification;
use Doctrine\ORM\EntityRepository;

class CertificationRepository extends EntityRepository
{
    public function loadCertificationByUserId($userid): array
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery();

        return $qb->execute();
    }
}
