<?php

namespace App\Repository;

use App\Entity\PivotsLong;
use Doctrine\ORM\EntityRepository;

class PivotsLongRepository extends EntityRepository
{
 	public function loadPivotsLongByID($id)
    {
        return $this->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllPivotsLong(): array
	{
    	$conn = $this->getEntityManager()->getConnection();

    	$sql = "SELECT * FROM pivotslong e ORDER BY e.id DESC ";
    	$stmt = $conn->prepare($sql);
    	$stmt->execute();

    	// returns an array of arrays (i.e. a raw data set)
    	return $stmt->fetchAll();
	}   
}
