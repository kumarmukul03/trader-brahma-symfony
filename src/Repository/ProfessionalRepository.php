<?php

namespace App\Repository;

use App\Entity\Professional;
use Doctrine\ORM\EntityRepository;

class ProfessionalRepository extends EntityRepository
{
    public function loadProfessionalByUserId($userid): array
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery();

        return $qb->execute();
    }
}
