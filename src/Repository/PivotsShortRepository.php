<?php

namespace App\Repository;

use App\Entity\PivotsShort;
use Doctrine\ORM\EntityRepository;

class PivotsShortRepository extends EntityRepository
{
 	public function loadPivotsShortByID($id)
    {
        return $this->createQueryBuilder('u')
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllPivotsLong(): array
	{
    	$conn = $this->getEntityManager()->getConnection();

    	$sql = "SELECT * FROM pivotsshort e ORDER BY e.id DESC ";
    	$stmt = $conn->prepare($sql);
    	$stmt->execute();

    	// returns an array of arrays (i.e. a raw data set)
    	return $stmt->fetchAll();
	}   
}
