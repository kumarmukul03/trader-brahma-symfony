<?php

namespace App\Repository;

use App\Entity\Achievement;
use Doctrine\ORM\EntityRepository;

class AchievementRepository extends EntityRepository
{
    public function loadAchievementByUserId($userid): array
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery();

        return $qb->execute();
    }
}
