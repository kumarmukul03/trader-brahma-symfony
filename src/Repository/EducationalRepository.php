<?php

namespace App\Repository;

use App\Entity\Educational;
use Doctrine\ORM\EntityRepository;

class EducationalRepository extends EntityRepository
{
    public function loadEducationalByUserId($userid): array
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery();

        return $qb->execute();
    }
}
