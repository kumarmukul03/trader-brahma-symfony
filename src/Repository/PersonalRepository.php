<?php

namespace App\Repository;

use App\Entity\Personal;
use Doctrine\ORM\EntityRepository;

class PersonalRepository extends EntityRepository
{
 	public function loadPersonalByUserId($userid)
    {
        return $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllPersonal(): array
	{
    	$conn = $this->getEntityManager()->getConnection();

    	$sql = "SELECT * FROM personal e ORDER BY e.id DESC ";
    	$stmt = $conn->prepare($sql);
    	$stmt->execute();

    	// returns an array of arrays (i.e. a raw data set)
    	return $stmt->fetchAll();
	}   
}
