<?php

namespace App\Repository;

use App\Entity\Decleration;
use Doctrine\ORM\EntityRepository;

class DeclerationRepository extends EntityRepository
{
    public function loadDeclerationByUserId($userid)
    {
        return $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
