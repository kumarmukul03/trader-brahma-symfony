<?php

namespace App\Repository;

use App\Entity\Brokerconnect;
use Doctrine\ORM\EntityRepository;

class BrokerconnectRepository extends EntityRepository
{
 	public function loadBrokerconnectByUserId($userid)
    {
        return $this->createQueryBuilder('u')
            ->where('u.userid = :userid')
            ->setParameter('userid', $userid)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllBroker(): array
	{
    	$conn = $this->getEntityManager()->getConnection();

    	$sql = "SELECT * FROM brokerconnect e ORDER BY e.id DESC ";
    	$stmt = $conn->prepare($sql);
    	$stmt->execute();

    	// returns an array of arrays (i.e. a raw data set)
    	return $stmt->fetchAll();
	}   
}
