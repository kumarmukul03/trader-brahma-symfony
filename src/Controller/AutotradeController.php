<?php

namespace App\Controller;

use App\Entity\Brokerconnect;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use App\phpkiteconnect\KiteConnect;

class AutotradeController extends AbstractController
{
    /**
     * @Route("/autotrade", name="autotrade")
     */
    public function index(Request $request)
    {
        if (!$request->isMethod('GET')) {
            return new RedirectResponse("https://kite.trade/connect/login?v=3&api_key=11f45k9ld652m3pu");
        }
        else
        {
            $userid = $this->getUser()->getId();

            // Initialise.
            $kite = new KiteConnect("11f45k9ld652m3pu");
        
            // Assuming you have obtained the `request_token`
	        // after the auth flow redirect by redirecting the
	        // user to $kite->login_url()
	    try {
		    $user = $kite->generateSession($request->query->get('request_token'), "l2odw32l8wj2966tyy8rvfftz2av7dcm");
		    echo "Authentication successful. \n";
		    print_r($user);
		    $kite->setAccessToken($user->access_token);
	    } catch(Exception $e) {
		    echo "Authentication failed: ".$e->getMessage();
		    throw $e;
        }

        }
            
        $broker = new Brokerconnect();
        $broker->setUserid($userid);
        $broker->setAccesstoken($user->access_token);

        // save the Personal Data!
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($broker);
        $entityManager->flush();


        return $this->render('autotrade/index.html.twig', [
            'controller_name' => 'AutotradeController',
        ]);
    }
}
