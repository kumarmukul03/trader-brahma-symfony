<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TraderoomController extends AbstractController
{
    /**
     * @Route("/traderoom", name="traderoom")
     */
    public function index()
    {
        return $this->render('traderoom/index.html.twig', [
            'controller_name' => 'TraderoomController',
        ]);
    }
}
