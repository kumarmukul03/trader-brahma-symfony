<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use App\Entity\PivotsLong;
use App\Entity\PivotsShort;

class LibraryController extends AbstractController
{
    /**
     * @Route("/home/library", name="library")
     */
    public function index()
    {



        return $this->render('library/index.html.twig');
    }

    /**
     * @Route("/upload_excel", name="upload_excel")
     */
    public function upload(Request $request)
    {
        $inputFileName = $request->files->get('file');

        $reader1 = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $reader1->setLoadSheetsOnly("LONG");

        $spreadsheet = $reader1->load($inputFileName);

        $worksheet = $spreadsheet->getActiveSheet()->toArray();  // get active worksheet
        $count = 1;
        foreach ($worksheet AS $rows) {

            $pivots = new PivotsLong();
            $pivots->setSlno($rows[0]);
            $pivots->setName($rows[1]);
            $pivots->setTopcenter($rows[2]);
            $pivots->setBottomcenter($rows[3]);
            $pivots->setS3($rows[4]);
            $pivots->setR3($rows[5]);
            $pivots->setS4($rows[6]);
            $pivots->setR4($rows[7]);
            $pivots->setS5($rows[8]);
            $pivots->setR5($rows[9]);
            $pivots->setS6($rows[10]);
            $pivots->setR6($rows[11]);
            $pivots->setConfluenece($rows[12]);

            if($count!=1)
            {
                //Initialize cURL.
                $ch = curl_init();

                $headers = array(
                    "X-Kite-Version: 3",
                    "Authorization: token 11f45k9ld652m3pu:RZuybCB0C6mrpMHhdQ9nVoaNyA7H6Dlf"
                    );
 
                //Set the URL that you want to GET by using the CURLOPT_URL option.
                curl_setopt($ch, CURLOPT_URL, 'https://api.kite.trade/quote/ohlc?i=NSE:'.$rows[1]);
 
                //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
                //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_HEADER, 0);

                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

                // Timeout in seconds
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
 
                //Execute the request.
                $data = curl_exec($ch);
 
                //Close the cURL handle.
                curl_close($ch);

                $result = json_decode($data, true);
 
                //Print the data out onto the page.
                echo $data;
                $pivots->setInstr_token($result['data']['NSE:'.$rows[1]]['instrument_token']);

                // save the Personal Data!
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($pivots);
                $entityManager->flush();
            }

            $count++;
            
        }

        // For Pivot Short
        $reader2 = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $reader2->setLoadSheetsOnly("SHORT");

        $spreadsheet = $reader2->load($inputFileName);

        $worksheet = $spreadsheet->getActiveSheet()->toArray();  // get active worksheet
        $count = 1;
        foreach ($worksheet AS $rows) {

            $pivots = new PivotsShort();
            $pivots->setSlno($rows[0]);
            $pivots->setName($rows[1]);
            $pivots->setTopcenter($rows[2]);
            $pivots->setBottomcenter($rows[3]);
            $pivots->setS3($rows[4]);
            $pivots->setR3($rows[5]);
            $pivots->setS4($rows[6]);
            $pivots->setR4($rows[7]);
            $pivots->setS5($rows[8]);
            $pivots->setR5($rows[9]);
            $pivots->setS6($rows[10]);
            $pivots->setR6($rows[11]);
            $pivots->setConfluenece($rows[12]);

            if($count!=1)
            {

                //Initialize cURL.
                $ch = curl_init();

                $headers = array(
                    "X-Kite-Version: 3",
                    "Authorization: token 11f45k9ld652m3pu:RZuybCB0C6mrpMHhdQ9nVoaNyA7H6Dlf"
                    );
 
                //Set the URL that you want to GET by using the CURLOPT_URL option.
                curl_setopt($ch, CURLOPT_URL, 'https://api.kite.trade/quote/ohlc?i=NSE:'.$rows[1]);
 
                //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
                //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_HEADER, 0);

                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

                // Timeout in seconds
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
 
                //Execute the request.
                $data = curl_exec($ch);
 
                //Close the cURL handle.
                curl_close($ch);

                $result = json_decode($data, true);
 
                //Print the data out onto the page.
                echo $data;
                $pivots->setInstr_token($result['data']['NSE:'.$rows[1]]['instrument_token']);

                // save the Personal Data!
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($pivots);
                $entityManager->flush();
            }

            $count++;
            
        }

        return $this->render('library/index.html.twig');
    }
}
