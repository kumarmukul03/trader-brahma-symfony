<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PapertradeController extends AbstractController
{
    /**
     * @Route("/home/papertrade", name="papertrade")
     */
    public function index()
    {
        return $this->render('papertrade/index.html.twig', [
            'controller_name' => 'PapertradeController',
        ]);
    }
}
