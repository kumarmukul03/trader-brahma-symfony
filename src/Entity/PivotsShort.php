<?php
// src/Entity/PivotsShort.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pivotsshort")
 * @ORM\Entity(repositoryClass="App\Repository\PivotsShortRepository")
 */
class PivotsShort
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $slno;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $topcenter;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $bottomcenter;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $s3;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $r3;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $s4;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $r4;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $s5;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $r5;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $s6;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $r6;

    /**
     * @ORM\Column(type="boolean")
     */
    private $confluenece;

    /**
     * @ORM\Column(type="string", length = 30)
     */
    private $instr_token;

    /**
     * @ORM\Column(type="boolean")
     */
    private $openlbc;

    /**
     * @ORM\Column(type="boolean")
     */
    private $opengtc;

    /**
     * @ORM\Column(type="boolean")
     */
    private $openles4;

    /**
     * @ORM\Column(type="boolean")
     */
    private $ltpger3;

    /**
     * @ORM\Column(type="boolean")
     */
    private $ltplebc;

    /**
     * @ORM\Column(type="boolean")
     */
    private $ltples4;

    /**
     * @ORM\Column(type="boolean")
     */
    private $ltpger4;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $sl;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $target;

    /**
     * @ORM\Column(type="string", length = 45)
     */
    private $triggername;

    /**
     * @ORM\Column(type="boolean")
     */
    private $position;

    /**
     * @ORM\Column(type="boolean")
     */
    private $slhit;

    /**
     * @ORM\Column(type="boolean")
     */
    private $targethit;


    public function __construct()
    {
        
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    

    /**
     * Get the value of confluenece
     */ 
    public function getConfluenece()
    {
        return $this->confluenece;
    }

    /**
     * Set the value of confluenece
     *
     * @return  self
     */ 
    public function setConfluenece($confluenece)
    {
        $this->confluenece = $confluenece;

        return $this;
    }

    /**
     * Get the value of s5
     */ 
    public function getS5()
    {
        return $this->s5;
    }

    /**
     * Set the value of s5
     *
     * @return  self
     */ 
    public function setS5($s5)
    {
        $this->s5 = $s5;

        return $this;
    }

    /**
     * Get the value of s4
     */ 
    public function getS4()
    {
        return $this->s4;
    }

    /**
     * Set the value of s4
     *
     * @return  self
     */ 
    public function setS4($s4)
    {
        $this->s4 = $s4;

        return $this;
    }

    /**
     * Get the value of s3
     */ 
    public function getS3()
    {
        return $this->s3;
    }

    /**
     * Set the value of s3
     *
     * @return  self
     */ 
    public function setS3($s3)
    {
        $this->s3 = $s3;

        return $this;
    }

    /**
     * Get the value of r5
     */ 
    public function getR5()
    {
        return $this->r5;
    }

    /**
     * Set the value of r5
     *
     * @return  self
     */ 
    public function setR5($r5)
    {
        $this->r5 = $r5;

        return $this;
    }

    /**
     * Get the value of r4
     */ 
    public function getR4()
    {
        return $this->r4;
    }

    /**
     * Set the value of r4
     *
     * @return  self
     */ 
    public function setR4($r4)
    {
        $this->r4 = $r4;

        return $this;
    }

    /**
     * Get the value of r3
     */ 
    public function getR3()
    {
        return $this->r3;
    }

    /**
     * Set the value of r3
     *
     * @return  self
     */ 
    public function setR3($r3)
    {
        $this->r3 = $r3;

        return $this;
    }

    /**
     * Get the value of bottomcenter
     */ 
    public function getBottomcenter()
    {
        return $this->bottomcenter;
    }

    /**
     * Set the value of bottomcenter
     *
     * @return  self
     */ 
    public function setBottomcenter($bottomcenter)
    {
        $this->bottomcenter = $bottomcenter;

        return $this;
    }

    /**
     * Get the value of topcenter
     */ 
    public function getTopcenter()
    {
        return $this->topcenter;
    }

    /**
     * Set the value of topcenter
     *
     * @return  self
     */ 
    public function setTopcenter($topcenter)
    {
        $this->topcenter = $topcenter;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of slno
     */ 
    public function getSlno()
    {
        return $this->slno;
    }

    /**
     * Set the value of slno
     *
     * @return  self
     */ 
    public function setSlno($slno)
    {
        $this->slno = $slno;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of r6
     */ 
    public function getR6()
    {
        return $this->r6;
    }

    /**
     * Set the value of r6
     *
     * @return  self
     */ 
    public function setR6($r6)
    {
        $this->r6 = $r6;

        return $this;
    }

    /**
     * Get the value of s6
     */ 
    public function getS6()
    {
        return $this->s6;
    }

    /**
     * Set the value of s6
     *
     * @return  self
     */ 
    public function setS6($s6)
    {
        $this->s6 = $s6;

        return $this;
    }

    /**
     * Get the value of instr_token
     */ 
    public function getInstr_token()
    {
        return $this->instr_token;
    }

    /**
     * Set the value of instr_token
     *
     * @return  self
     */ 
    public function setInstr_token($instr_token)
    {
        $this->instr_token = $instr_token;

        return $this;
    }

    /**
     * Get the value of openlbc
     */ 
    public function getOpenlbc()
    {
        return $this->openlbc;
    }

    /**
     * Set the value of openlbc
     *
     * @return  self
     */ 
    public function setOpenlbc($openlbc)
    {
        $this->openlbc = $openlbc;

        return $this;
    }

    /**
     * Get the value of opengtc
     */ 
    public function getOpengtc()
    {
        return $this->opengtc;
    }

    /**
     * Set the value of opengtc
     *
     * @return  self
     */ 
    public function setOpengtc($opengtc)
    {
        $this->opengtc = $opengtc;

        return $this;
    }

    /**
     * Get the value of openles4
     */ 
    public function getOpenles4()
    {
        return $this->openles4;
    }

    /**
     * Set the value of openles4
     *
     * @return  self
     */ 
    public function setOpenles4($openles4)
    {
        $this->openles4 = $openles4;

        return $this;
    }

    /**
     * Get the value of ltpger3
     */ 
    public function getLtpger3()
    {
        return $this->ltpger3;
    }

    /**
     * Set the value of ltpger3
     *
     * @return  self
     */ 
    public function setLtpger3($ltpger3)
    {
        $this->ltpger3 = $ltpger3;

        return $this;
    }

    /**
     * Get the value of ltplebc
     */ 
    public function getLtplebc()
    {
        return $this->ltplebc;
    }

    /**
     * Set the value of ltplebc
     *
     * @return  self
     */ 
    public function setLtplebc($ltplebc)
    {
        $this->ltplebc = $ltplebc;

        return $this;
    }

    /**
     * Get the value of ltples4
     */ 
    public function getLtples4()
    {
        return $this->ltples4;
    }

    /**
     * Set the value of ltples4
     *
     * @return  self
     */ 
    public function setLtples4($ltples4)
    {
        $this->ltples4 = $ltples4;

        return $this;
    }

    /**
     * Get the value of ltpger4
     */ 
    public function getLtpger4()
    {
        return $this->ltpger4;
    }

    /**
     * Set the value of ltpger4
     *
     * @return  self
     */ 
    public function setLtpger4($ltpger4)
    {
        $this->ltpger4 = $ltpger4;

        return $this;
    }

    /**
     * Get the value of sl
     */ 
    public function getSl()
    {
        return $this->sl;
    }

    /**
     * Set the value of sl
     *
     * @return  self
     */ 
    public function setSl($sl)
    {
        $this->sl = $sl;

        return $this;
    }

    /**
     * Get the value of target
     */ 
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set the value of target
     *
     * @return  self
     */ 
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get the value of triggername
     */ 
    public function getTriggername()
    {
        return $this->triggername;
    }

    /**
     * Set the value of triggername
     *
     * @return  self
     */ 
    public function setTriggername($triggername)
    {
        $this->triggername = $triggername;

        return $this;
    }

    /**
     * Get the value of position
     */ 
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set the value of position
     *
     * @return  self
     */ 
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get the value of slhit
     */ 
    public function getSlhit()
    {
        return $this->slhit;
    }

    /**
     * Set the value of slhit
     *
     * @return  self
     */ 
    public function setSlhit($slhit)
    {
        $this->slhit = $slhit;

        return $this;
    }

    /**
     * Get the value of targethit
     */ 
    public function getTargethit()
    {
        return $this->targethit;
    }

    /**
     * Set the value of targethit
     *
     * @return  self
     */ 
    public function setTargethit($targethit)
    {
        $this->targethit = $targethit;

        return $this;
    }
}
?>