<?php
// src/Entity/Educational.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="educational")
 * @ORM\Entity(repositoryClass="App\Repository\EducationalRepository")
 */
class Educational
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $userid;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $course;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $college;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $marks;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @Assert\NotBlank(message="Please, upload the certificate.") 
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg" }) 
     */
    private $certificate;


    public function __construct()
    {
        
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserid()
    {
        return $this->userid;
    }

    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    public function getCourse()
    {
        return $this->course;
    }

    public function setCourse($course)
    {
        $this->course = $course;
    }

    public function getCollege()
    {
        return $this->college;
    }

    public function setCollege($college)
    {
        $this->college = $college;
    }

    public function getMarks()
    {
        return $this->marks;
    }

    public function setMarks($marks)
    {
        $this->marks = $marks;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }

    public function getCertificate()
    {
        return $this->certificate;
    }

    public function setCertificate($certificate)
    {
        $this->certificate = $certificate;
    }
}
?>