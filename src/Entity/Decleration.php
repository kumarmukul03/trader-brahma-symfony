<?php
// src/Entity/Decleration.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="decleration")
 * @ORM\Entity(repositoryClass="App\Repository\DeclerationRepository")
 */
class Decleration
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $userid;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @Assert\NotBlank(message="Please, upload the profile image.") 
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg" }) 
     */
    private $profileimg;

    /**
     * @ORM\Column(type="string", length=100)
     *
     * @Assert\NotBlank(message="Please, upload the signature.") 
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg" }) 
     */
    private $signimg;


    public function __construct()
    {
        
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserid()
    {
        return $this->userid;
    }

    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    public function getProfileimg()
    {
        return $this->profileimg;
    }

    public function setProfileimg($profileimg)
    {
        $this->profileimg = $profileimg;
    }

    public function getSignimg()
    {
        return $this->signimg;
    }

    public function setSignimg($signimg)
    {
        $this->signimg = $signimg;
    }
}
?>