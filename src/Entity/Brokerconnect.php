<?php
// src/Entity/Brokerconnect.php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="brokerconnect")
 * @ORM\Entity(repositoryClass="App\Repository\BrokerconnectRepository")
 */
class Brokerconnect
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $brokerconnectid;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $userid;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $brokername;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $apikey;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $apisecret;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $accesstoken;


    public function __construct()
    {
        
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function getBrokerconnectid()
    {
        return $this->brokerconnectid;
    }

    public function getUserid()
    {
        return $this->userid;
    }

    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    public function getBrokername()
    {
        return $this->brokername;
    }

    public function setBrokername($brokername)
    {
        $this->brokername = $brokername;
    }

    public function getApikey()
    {
        return $this->apikey;
    }

    public function setApikey($apikey)
    {
        $this->apikey = $apikey;
    }

    public function getApisecret()
    {
        return $this->apisecret;
    }

    public function setApisecret($apisecret)
    {
        $this->apisecret = $apisecret;
    }

    public function getAccesstoken()
    {
        return $this->accesstoken;
    }

    public function setAccesstoken($accesstoken)
    {
        $this->accesstoken = $accesstoken;
    }
}
?>