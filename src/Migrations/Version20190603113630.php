<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190603113630 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pivotslong ADD instr_token VARCHAR(30) NOT NULL, ADD opengtc TINYINT(1) NOT NULL, ADD openlbc TINYINT(1) NOT NULL, ADD openger4 TINYINT(1) NOT NULL, ADD ltples3 TINYINT(1) NOT NULL, ADD ltpgetc TINYINT(1) NOT NULL, ADD ltples4 TINYINT(1) NOT NULL, ADD ltpger4 TINYINT(1) NOT NULL, ADD sl NUMERIC(8, 2) NOT NULL, ADD target NUMERIC(8, 2) NOT NULL, ADD triggername VARCHAR(45) NOT NULL, ADD position TINYINT(1) NOT NULL, ADD slhit TINYINT(1) NOT NULL, ADD targethit TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE pivotsshort ADD instr_token VARCHAR(30) NOT NULL, ADD openlbc TINYINT(1) NOT NULL, ADD opengtc TINYINT(1) NOT NULL, ADD openles4 TINYINT(1) NOT NULL, ADD ltpger3 TINYINT(1) NOT NULL, ADD ltplebc TINYINT(1) NOT NULL, ADD ltples4 TINYINT(1) NOT NULL, ADD ltpger4 TINYINT(1) NOT NULL, ADD sl NUMERIC(8, 2) NOT NULL, ADD target NUMERIC(8, 2) NOT NULL, ADD triggername VARCHAR(45) NOT NULL, ADD position TINYINT(1) NOT NULL, ADD slhit TINYINT(1) NOT NULL, ADD targethit TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pivotslong DROP instr_token, DROP opengtc, DROP openlbc, DROP openger4, DROP ltples3, DROP ltpgetc, DROP ltples4, DROP ltpger4, DROP sl, DROP target, DROP triggername, DROP position, DROP slhit, DROP targethit');
        $this->addSql('ALTER TABLE pivotsshort DROP instr_token, DROP openlbc, DROP opengtc, DROP openles4, DROP ltpger3, DROP ltplebc, DROP ltples4, DROP ltpger4, DROP sl, DROP target, DROP triggername, DROP position, DROP slhit, DROP targethit');
    }
}
