/* set global variable i */

var edu=1;
var soft=1;
var cert=1;
var exp=1;
var proj=1;
var ach=1;

function increment(id){
     if(id=='edu')
     {
          edu +=1;
     }
     else if(id=='soft')
     {
          soft +=1;
     }
     else if(id=='cert')
     {
          cert +=1;
     }
     else if(id=='exp')
     {
          exp +=1;
     }
     else if(id=='proj')
     {
          proj +=1;
     }
     else if(id=='ach')
     {
          ach +=1;
     }                       
}
function decrement(id){
     if(id=='edu')
     {
          edu -=1;
     }
     else if(id=='soft')
     {
          soft -=1;
     }
     else if(id=='cert')
     {
          cert -=1;
     }
     else if(id=='exp')
     {
          exp -=1;
     }
     else if(id=='proj')
     {
          proj -=1;
     }
     else if(id=='ach')
     {
          ach -=1;
     }
}


/* 
---------------------------------------------

function to remove fom elements dynamically
---------------------------------------------

*/

function removeElement(parentDiv, childDiv){
     var i = 0;
     if(childDiv=='edu')
     {
          i=edu;
     }
     else if(childDiv=='soft')
     {
          i=soft;
     }
     else if(childDiv=='cert')
     {
          i=cert;
     }
     else if(childDiv=='exp')
     {
          i=exp;
     }
     else if(childDiv=='proj')
     {
          i=proj;
     }
     else if(childDiv=='ach')
     {
          i=ach;
     }

     if (childDiv == parentDiv) {
          alert("The parent div cannot be removed.");
     }
     else if (document.getElementById(childDiv + String(i - 1))) {     
          var child = document.getElementById(childDiv + String(i - 1));
          var parent = document.getElementById(parentDiv);
          parent.removeChild(child);
          decrement(childDiv);
          document.getElementById(event.srcElement.id).setAttribute("onclick", "removeElement('"+parentDiv+"','"+ childDiv +"')");
     }
     else {
          alert("Child div has already been removed or does not exist.");
          return false;
     }
}

/* 
 ----------------------------------------------------------------------------
 
 functions that will be called upon, when user click on the Plus Sign
 
 ---------------------------------------------------------------------------
 */

function addElement(idn, nam)
{
     var i = 0;
     if(nam=='edu')
     {
          i=edu;
     }
     else if(nam=='soft')
     {
          i=soft;
     }
     else if(nam=='cert')
     {
          i=cert;
     }
     else if(nam=='exp')
     {
          i=exp;
     }
     else if(nam=='proj')
     {
          i=proj;
     }
     else if(nam=='ach')
     {
          i=ach;
     }

     var elm = document.getElementById(nam).cloneNode( true );
     elm.setAttribute("id", nam+i);
     document.getElementById(idn).appendChild(elm);
     document.getElementById(nam+"_remove").setAttribute("onclick", "removeElement('"+idn+"','"+ nam +"')");
     increment(nam);
}

function showImage(src,target) {
  var fr=new FileReader();
  // when image is loaded, set the src of the image where you want to display it
  fr.onload = function(e) { target.src = this.result; };
  src.addEventListener("change",function() {
    // fill fr with image data    
    fr.readAsDataURL(src.files[0]);
  });
}

var src = document.getElementById("profile-img");
var target = document.getElementById("profile-img-tag");
showImage(src,target);

var src = document.getElementById("sign-img");
var target = document.getElementById("sign-img-tag");
showImage(src,target);

function setDefault() {
     document.getElementById("profile-img-tag").src = '/images/img_avatar.png';
     document.getElementById("sign-img-tag").src = '/images/signature.png';
}
